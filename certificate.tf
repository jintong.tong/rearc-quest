resource "aws_iam_server_certificate" "ssl-cert" {
  name = "ssl-cert"
  certificate_body = file("public.pem")
  private_key = file("private.pem")
}