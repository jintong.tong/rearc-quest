# Use an official Node runtime as a parent image
FROM node:10

# Set the working directory to /app
WORKDIR '/app'

# Copy package.json to the working directory
COPY package.json .


# Copying the rest of the code to the working directory
COPY . .

# Make port 3000 available to the world outside this container
EXPOSE 3000

RUN npm install

ENV SECRET_WORD TwelveFactor

# Run index.js when the container launches
CMD ["npm", "start"]