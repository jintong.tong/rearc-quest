provider "aws" {
  region  = "us-east-1"
}

resource "aws_ecr_repository" "quest_ecr_repo" {
  name = "quest-ecr-repo"
}